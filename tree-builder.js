let _ = require('lodash');
var GlobalConst = require('../config/global');

module.exports = {
    createTreeData,
    createNestedArray,
    detectMultiLanguages
}

function createTreeData(data) {
    return new Promise(async (resolve, reject) => {
        let tree = [],
            mappedArr = {},
            arrElem,
            mappedElem;

        // First map the nodes of the array to an object -> create a hash table.
        for (var i = 0, len = data.length; i < len; i++) {
            arrElem = data[i];
            mappedArr[arrElem.id] = arrElem;
            mappedArr[arrElem.id]['reasons'] = [];
        }
        for (var id in mappedArr) {
            if (mappedArr.hasOwnProperty(id)) {
                mappedElem = mappedArr[id];
                // If the element is not at the root level, add it to its parent array of children.
                if (mappedElem.parentID && typeof mappedArr[mappedElem['parentID']]['reasons'] != "undefined") {
                    mappedArr[mappedElem['parentID']]['reasons'].push(mappedElem);
                }
                // If the element is at the root level, add it to first level elements array.
                else {
                    tree.push(mappedElem);
                }
            }
        }
        return resolve(tree);
    });
}

function createNestedArray(keys, value, obj) {
    obj = obj || {}
    keys = typeof keys === 'string'
        ? keys.match(/\w+/g)
        : Array.prototype.slice.apply(keys)
    keys.reduce(function (obj, key, index) {
        obj[key] = index === keys.length - 1
            ? value
            : typeof obj[key] === 'object' && obj !== null
                ? obj[key]
                : {}
        return obj[key]
    }, obj)
    return obj
}

function detectMultiLanguages(data, language = GlobalConst.DEFAULT_LANGUAGE) {
    let ignoreField = ['id', 'lang', 'createdAt', 'updatedAt'];
    if (_.isArray(data) && data.length) {
        for (let item of data) {
            detectMultiLanguages(item, language);
        }
    }
    if (_.isObject(data) && !_.isEmpty(data)) {
        for (let param in data) {
            if (_.isArray(data[param]) && data.length) {
                /*
                * Check child is array
                */
                for (let item of data[param]) {
                    detectMultiLanguages(item, language);
                }
            }

            if (_.isObject(data[param]) && !_.isEmpty(data)) {
                /*
                * Check child is array
                */
                detectMultiLanguages(data[param], language);
            }
            if (param === 'langs' && data.langs.length) {
                /*
                * Get object language with language require
                */
                const langObject = data.langs.filter(lang => lang.lang === language);
                if (!langObject.length) {
                    delete data.langs;
                    continue;
                }
                for (let field in langObject[0]) {
                    if (typeof data[field] != "undefined" && !ignoreField.includes(field)) {
                        data[field] = langObject[0][field];
                    }
                }
                delete data.langs;
            }
        }
    }
    return data;
}
