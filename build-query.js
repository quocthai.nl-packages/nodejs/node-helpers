const _ = require('lodash');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const GlobalConst = require('../config/global');
/**
 *
 * @param filter and paging
 * @param rootQuery
 * @returns {{hasPaging: boolean, query: *}}
 */
module.exports = (filter, rootQuery = {}) => {
    let query = _.cloneDeep(rootQuery);
    let dataParams = _.cloneDeep(filter);
    let result = {
        hasPaging: false,
        query: query
    };

    if (!_.isEmpty(dataParams)) {
        /*
        * Check Order
        */
        if (_.has(dataParams, 'order_by') && _.has(dataParams, 'order_type')) {
            if (!query.order) query.order = [];
            let order = dataParams.order_by.split(",");
            order.push(dataParams.order_type);
            query.order.push(order);
        }
        delete dataParams.order_by;
        delete dataParams.order_type;
        /*
        * Check Attributes
        */
        if (_.has(dataParams, 'attributes')) {
            let attributes = dataParams.attributes.split(",");
            query.attributes = attributes;
        }
        delete dataParams.attributes;

        /*
        * Check limit and offset
        */
        if (_.has(dataParams, 'limit') && _.has(dataParams, 'page')) {
            query.limit = dataParams.limit;
            query.offset = dataParams.limit * (dataParams.page - 1);
            result.hasPaging = true;
        }
        delete dataParams.limit;
        delete dataParams.page;
        /*
        * Build Where query
        */
        let whereArray = [];

        if (_.has(dataParams, 'or')) {
            let orArray = [];
            let parseData = _.isString(dataParams.or) ? JSON.parse(dataParams.or) : dataParams.or;
            for (let item in parseData) {
                orArray.push(Sequelize.where(Sequelize.fn('lower', Sequelize.col(item)), 'LIKE', `%${parseData[item]}%`))
            }

            whereArray.push({
                [Op.or] : orArray
            });
            delete dataParams.or;
        }

        for (let field in dataParams) {

            if (typeof field == 'string' && field.includes('|')) {
                /*
                * Check Field
                */
                let operator = field.split('|');

                if (operator[1] == 'like') {
                    let column = operator[0];
                    if (operator[0].includes('.')) {
                        /*
                        * Check filter by model
                        */
                        let {column: column, isJsonB: isJsonB} = generateColumnField(operator[0]);
                        if (isJsonB) {
                            /*
                            * Filter with JSONB column
                            */
                            whereArray.push({
                                [column] : {
                                    [Op.iLike]: `%${dataParams[field].toLowerCase()}%`
                                }
                            });
                        } else {
                            /*
                            * Filter with Model
                            */
                            whereArray.push(Sequelize.where(Sequelize.fn('lower', Sequelize.col(column)), 'LIKE', `%${dataParams[field].toLowerCase()}%`));
                        }

                    } else {
                        whereArray.push(Sequelize.where(Sequelize.fn('lower', Sequelize.col(column)), 'LIKE', `%${dataParams[field].toLowerCase()}%`));
                    }
                } else {
                    let value = dataParams[field];
                    let column = operator[0];
                    if (operator[0].includes('.')) {
                        /*
                        * Check filter by model
                        */
                        let {column: column, isJsonB: isJsonB} = generateColumnField(operator[0]);
                        if (isJsonB) {
                            whereArray.push({
                                [column] :  {
                                    [Op[operator[1]]]: value
                                }
                            });
                        } else {
                            /*
                            * TODO: Not support with model and Operators
                            */
                            whereArray.push(Sequelize.where(Sequelize.col(column), Op[operator[1]], value));
                        }
                    } else {
                        if (['notIn', 'between', 'notBetween', 'in'].includes(operator[1])) {
                            value = _.isString(value) ? JSON.parse(value) : value;
                        }
                        whereArray.push({
                            [column]: {
                                [Op[operator[1]]]: value
                            }
                        });
                    }
                }
            } else if (typeof field == 'string' && !field.includes('|') && field.includes('.')) {
                /*
                * Check filter with JSONB column
                */
                let {column: column, isJsonB: isJsonB} = generateColumnField(field);
                if (isJsonB) {
                    whereArray.push({
                        [field]: dataParams[field]
                    });
                } else {
                    whereArray.push(Sequelize.where(Sequelize.col(column), dataParams[field]));
                }
            } else {
                whereArray.push({
                    [field]: dataParams[field]
                });
            }
        }
        // add whereArray to query.where[Op.and]
        if (whereArray.length > 0) {
            if (!query.where) query.where = {};
            if (!query.where[Op.and]) {
                query.where[Op.and] = [];
            }
            // if query.where[Op.and] is object => convert to array of object
            if (!_.isArray(query.where[Op.and])) {
                query.where[Op.and] = [query.where[Op.and]];
            }
            query.where[Op.and] = query.where[Op.and].concat(whereArray);
        }
        result.query = query;
    }
    return result;
};

function generateColumnField(field) {
    let model = field.split('.');
    let column = field;
    if (GlobalConst.JSONB_COLUMNS.includes(model[0])) {
        return {column: column, isJsonB: true};
    }
    if (model.length > 2) {
        column = '';
        for (let i in model) {
            if ((parseInt(i) + 1) < model.length) {
                column += model[i] + '->';
            }

            if ((parseInt(i) + 1) == model.length) {
                column = column.substring(0, column.length - 2);
                column += '.' + model[i];
            }

        }
    }
    return {column: column, isJsonB: false};
}
