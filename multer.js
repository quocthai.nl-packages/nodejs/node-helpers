var multer = require('multer');
const path = require('path');

module.exports = {
    detectFormDataUploadFile
}


function detectFormDataUploadFile() {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, path.join(__dirname, '../public/images/raw'))
        },
        filename: (req, file, cb) => {
            let fileName = file.originalname;
            fileName = fileName.split(".");
            cb(null, fileName[0] + '_' + Date.now() + '.' + fileName[1])
        }
    });
    return multer({storage: storage});
}

