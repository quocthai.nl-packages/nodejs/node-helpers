const _ = require('lodash');
const paginate = require('express-paginate');
const GlobalConst = require('../config/global');
const TreeBuilder = require('../helpers/tree-builder');

module.exports = (req, res, data = {}, code = GlobalConst.HTTP_OK) => {
    if (res.headersSent) return;
    const today = new Date();
    const gmt = today.toUTCString();
    let result = {
        name: `${GlobalConst.APP_NAME} v${GlobalConst.VERSION_IOS_AOS}`,
        code: code,
        message: 'Successful',
        created: gmt,
        data: data
    };
    if (code === GlobalConst.HTTP_OK) {
        /*
        * Check Pagination
        */
        if (_.has(data, 'count') && _.has(data, 'rows')) {
            const itemCount = data.count;
            const pageCount = Math.ceil(itemCount / req.query.limit);
            result.data = data.rows;
            result.pagination = {
                current_page: parseInt(req.query.page),
                last_page: pageCount,
                per_page: parseInt(req.query.limit),
                total_pages: pageCount,
                total_rows: itemCount,
                pages: paginate.getArrayPages(req)(3, pageCount, req.query.page)
            };
        }
        if (_.isString(data)) {
            result.message = data;
            delete result.data;
            delete result.pagination;
        } else if (data.data && data.message) {
            result.message = data.message;
            result.data = data.data;
        }
        /*
        * Detect Multi languages
        */
        if (typeof result.data != "undefined") {
            let dataMulti = JSON.parse(JSON.stringify(result.data));
            result.data = TreeBuilder.detectMultiLanguages(dataMulti, req.language);
        }
    } else {
        delete result.message;
        result.errMessage = data.message;
        if (_.isString(data)) {
            result.errMessage = data;
        }
        if (_.has(data, 'errors')) {
            result.errMessage = data.errors[0].message;
        }

        if (process.env.NODE_ENV === 'development') {
            result.data = data;
        } else {
            delete result.data;
            delete result.pagination;
        }

    }


    return res.status(code).send(result);
};
