module.exports = {
    getString
};

function getString(req, key) {
    if (req && req.t) {
        return req.t(key)
    } else {
        return key;
    }
}
