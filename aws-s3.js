let _ = require('lodash');
const config = require('../config/config').awsS3;
const fs = require('fs');
const S3 = require('aws-sdk/clients/s3');
const ThumbnailGenerator = require('video-thumbnail-generator').default;
const path = require('path');
const AWS = require('aws-sdk');
const moment = require('moment');
let XLSX = require('xlsx');
const unzipper = require('unzipper');
const findLib = require('find');

const a2mImages = require('./jimp')
const s3 = new S3({
    apiVersion: process.env.S3_API_VERSION,
    region: process.env.S3_REGION,
});
var Raven = require('raven');

module.exports = {
    uploadImageSingle,
    uploadImageArray,
    uploadVideoSingle,
    isImage,
    isVideo,
    isAudio,
    isPDFfile,
    isZipFile,
    createThumbnailVideo,
    uploadFileSingle,
    uploadFilesArray,
    setupAndSendToS3,
    getSignedCookie,
    readExcelfile,
    unzipFile,
    uploadFileFromFolder
}

function getSignedCookie() {
    return new Promise((resolve, reject) => {
        let expiry = moment().add(3, 'days');
        let policy = {
            'Statement': [{
                'Resource': 'http*://' + process.env.CLOUDFRONT_URL + '/*',
                'Condition': {
                    'DateLessThan': {'AWS:EpochTime': expiry.unix()}
                }
            }]
        };
        fs.readFile(path.join(__dirname, '../config/Cloudfront_key.txt'), {encoding: 'utf-8'}, (err, key) => {
            let signer = new AWS.CloudFront.Signer(process.env.CLOUDFRONT_KEY_PAIR_ID, key);

            var options = {url: "https://" + process.env.CLOUDFRONT_URL, policy: JSON.stringify(policy)};
            let data = {
                cookies: {},
                signedUrl: '',
                expiryDate: expiry.toISOString()
            }
            signer.getSignedCookie(options, (err, cookies) => {
                if (err) {
                    return reject(err);
                } else {
                    data.cookies = cookies
                    return data;
                }
            });
            signer.getSignedUrl(options, (err, params) => {
                if (err) {
                    return reject(err);
                } else {
                    data.signedUrl = _.trim(params, `https://${process.env.CLOUDFRONT_URL}/?`);
                    return data;
                }
            });
            return resolve(data);
        });
    })
}

async function uploadVideoSingle(file, folder) {
    if (!isVideo(file)) {
        return false;
    }
    //var processedVideo = a2mImages.noResize(file);
    var name = await setupAndSendToS3(file, file, folder);
    return name;
}

async function uploadImageArray(files, folder) {
    let newImages = [];
    //need to use this type of loop to process in sequence and await response
    for (const file of files) {
        let image = await uploadImageSingle(file, folder);
        if (!image) {
            break;
        }
        newImages.push(image);
    }

    return newImages;
}

async function uploadImageSingle(file, folder) {
    return new Promise(async (resolve, reject) => {
        setTimeout(async () => {
            let processedImages = await a2mImages.optimizeImage(file);
            if (!processedImages) {
                /*
                * Optimize image false
                */
                return resolve(false);
            }

            for (var i = 0; i < processedImages.length; i++) {
                var name = await setupAndSendToS3(file, processedImages[i], folder);
            }

            return resolve(name);
        }, 3000);
    })
}

async function uploadFilesArray(files, folder) {
    let newFiles = [];
    //need to use this type of loop to process in sequence and await response
    for (const file of files) {
        let fil = await uploadFileSingle(file, folder);
        if (!fil) {
            break;
        }
        newFiles.push(fil);
    }

    return newFiles;
}

async function uploadFileSingle(file, folder) {
    let name = await setupAndSendToS3(file, file, folder);
    return name;
}

function setupAndSendToS3(uploadFile, resize, folder) {
    return new Promise(async (resolve, reject) => {
        let fileName = resize.filename //uploadFile.filename + '.' + fileType[1];

        var params = {
            Bucket: process.env.S3_IMAGE_BUCKET,
            Key: folder + fileName,
            Body: fs.createReadStream(resize.path), //(uploadFile.path),
            ContentType: uploadFile.mimetype, // this will allow it to be viewed inline
            ACL: 'public-read' // will allow it to be accessable to view
        };

        await uploadToS3(params)
            .then(data => {
                //delete the local file
                const path = require('path');
                var rawPath = path.join(__dirname, '../public/images/raw/');
                var exportPath = path.join(__dirname, '../public/images/export/');
                if (fs.existsSync(rawPath + fileName)) {
                    fs.unlinkSync(rawPath + fileName) //(uploadFile.path);
                }
                if (fs.existsSync(exportPath + fileName)) {
                    fs.unlinkSync(exportPath + fileName) //(uploadFile.path);
                }
                //set the filename to return
                return resolve(data.Key);

            })
            .catch((error) => {
                if (process.env.NODE_ENV === 'production') {
                    /*
                    * Sentry
                    */
                    Raven.captureMessage(`Error upload file to S3`, {
                        extra: {
                            error: error.message,
                            data: params
                        }
                    });
                }
                console.log(`=====Error upload file to S3======: ${error.message}`);
                console.log(error);
                return resolve(false);
            });
    })
}

function uploadToS3(params) {
    //exec call to S3 as a promise to wait for response
    return s3.upload(params).promise();
}


function getExtension(mimetype) {
    var parts = mimetype.split('/');
    return parts[parts.length - 1];
}

function isImage(file) {
    var ext = getExtension(file.mimetype);
    const validImageTypes = ['jpg', 'jpeg', 'png'];
    if (!validImageTypes.includes(ext.toLowerCase())) {
        return false;
    }
    return true;
}

function isVideo(file) {
    var ext = getExtension(file.mimetype);
    const validVideoTypes = ['mp4'];
    if (!validVideoTypes.includes(ext.toLowerCase())) {
        return false;
    }
    return true;
}

function isAudio(file) {
    var ext = getExtension(file.mimetype);
    const validVideoTypes = ['mp3'];
    if (!validVideoTypes.includes(ext.toLowerCase())) {
        return false;
    }
    return true;
}

function isPDFfile(file) {
    var ext = getExtension(file.mimetype);
    const validVideoTypes = ['pdf'];
    if (!validVideoTypes.includes(ext.toLowerCase())) {
        return false;
    }
    return true;
}

function isZipFile(file) {
    let parts = file.originalname.split('.');
    let ext = parts[parts.length - 1];
    const matchTypes = ['zip'];
    if (!matchTypes.includes(ext.toLowerCase())) {
        return false;
    }
    return true;
}

function createThumbnailVideo(file, folder) {
    return new Promise(async (resolve, reject) => {
        const tg = new ThumbnailGenerator({
            sourcePath: file.path,
            thumbnailPath: path.join(__dirname, '../public/images/raw'),
        });
        return tg.generateOneByPercentCb(90, async (err, result) => {
            if (err != null) {
                console.log(err.message);
                return resolve(false);
            }
            var fileUpload = {
                mimetype: 'image/png',
                filename: result,
                path: path.join(__dirname, `../public/images/raw/${result}`)
            }
            var name = await setupAndSendToS3(fileUpload, fileUpload, folder);
            return resolve(name);
        });
    });
}

function unzipFile(fileName) {
    return new Promise(async (resolve, reject) => {
        let part = fileName.split('.');
        let patch = path.join(__dirname, `../public/images/raw/${part[0]}/`);
        let excelFile = null;
        fs.createReadStream(path.join(__dirname, `../public/images/raw/${fileName}`))
            .pipe(unzipper.Extract({path: patch}))
            .on('entry', entry => entry.autodrain())
            .promise()
            .then(() => {
                findLib.file(/\.xlsx$/, patch, function (files) {
                    if (files.length) {
                        excelFile = files[0];
                    }
                    fs.readdir(patch + 'images', (err, files) => {
                        return resolve({path: patch, excelFile: excelFile, images: files});
                    })
                })
            }, e => {
                console.log('== Error unzip file ====', e);
                return resolve(false);
            });
    });
}

function readExcelfile(fileName, isPath = false) {
    return new Promise(async (resolve, reject) => {
        let buf;
        if (isPath) {
            buf = fs.readFileSync(fileName);
        } else {
            buf = fs.readFileSync(path.join(__dirname, `../public/images/raw/${fileName}`));
        }
        let workbook = XLSX.read(buf, {type: 'buffer', cellDates: true});
        const sheet_name_list = workbook.SheetNames;
        let data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]], {defval: null});
        data = data.filter((item, index) => index > 2);
        return resolve(data);
    });
}

function uploadFileFromFolder(distFolderPath, s3Path) {
    return new Promise((resolve, reject) => {
        fs.readdir(distFolderPath, async (err, files) => {
            let uploaded = [];
            if (!files || files.length === 0) {
                console.log(`===== provided folder '${distFolderPath}' is empty or does not exist.======`);
                return;
            }

            // for each file in the directory
            for (const fileName of files) {

                // get the full path of the file
                const filePath = path.join(distFolderPath, fileName);

                // ignore if directory
                if (fs.lstatSync(filePath).isDirectory()) {
                    continue;
                }
                if (fileName === '.DS_Store') {
                    continue;
                } else {
                    let fileExtension = path.extname(fileName).toLowerCase();
                    if (fileExtension !== '.jpg' && fileExtension !== '.jpeg') {
                        return reject({message: `Unsupported file ${fileName}`})
                    } else {
                        let fileStatus = fs.statSync(`${distFolderPath}${fileName}`);
                        if (fileStatus && fileStatus.size && (fileStatus.size/1000) > 550 ) { // file size greater than 500kb
                            return reject({message: `${fileName} is oversize. Max allow is 500Kb`})
                        }
                    }
                }
                let processedImages = await a2mImages.optimizeImage({path: filePath, filename: fileName});
                if (!processedImages) {
                    /*
                    * Optimize image false
                    */
                    return resolve(false);
                }
                for (let i = 0; i < processedImages.length; i++) {
                    let part = fileName.split('.');
                    let name = await setupAndSendToS3({mimetype: part[1]}, processedImages[i], s3Path);
                    uploaded.push(name)
                }
            }
            return resolve(uploaded);
        });
    });


}
