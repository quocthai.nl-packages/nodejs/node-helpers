let _ = require('lodash');
let Validator = require('validatorjs');
var GlobalConst = require('./../config/global');

module.exports = (data, rules, language = GlobalConst.DEFAULT_LANGUAGE, isObject = false) => {
    return new Promise((resolve, reject) => {
        Validator.useLang(language);
        let validator = new Validator(data, rules);
        validator.passes(() => resolve(false));

        validator.fails(() => {
            let errors = validator.errors.all();
            if (isObject) {
                return resolve(errors);
            }
            let messageError = '';
            for (let param in errors) {
                messageError = errors[param][0];
                break;
            }
            return resolve(messageError);
        });
    })
}
