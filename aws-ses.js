const SES = require('aws-sdk/clients/ses');

const ses = new SES({
    apiVersion: process.env.SES_API_VERSION,
    region: process.env.SES_REGION,
});


async function sendEmail(email, raw = false) {
    if (!email) {
        return;
    }

    var params = {
        Destination: {
            ToAddresses: email.email
        },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8",
                    Data: email.body,
                },
            },
            Subject: {
                Charset: "UTF-8",
                Data: email.subject
            }
        },
        ReplyToAddresses: [],
        Source: process.env.SES_MAIL_SENDER,
    };

    if (raw) {
        let rawMailBody = `Subject: ${email.subject}\n`;
        rawMailBody = rawMailBody + `From: ${process.env.SES_MAIL_SENDER}\n`
        rawMailBody = rawMailBody + ``
        rawMailBody = rawMailBody + "MIME-Version: 1.0\n";
        rawMailBody = rawMailBody + "Content-Type: multipart/mixed; boundary=\"NextPart\"\n\n";
        rawMailBody = rawMailBody + "--NextPart\n";

        rawMailBody = rawMailBody + "Content-Type: text/html; charset=iso-8859-1\n";
        rawMailBody = rawMailBody + "\n";

        rawMailBody = rawMailBody + email.body;

        rawMailBody = rawMailBody + "--NextPart\n";


        params = {
            Destinations: email.email,
            RawMessage: {
                Data: rawMailBody
            },
            Source: process.env.SES_MAIL_SENDER, // Must be verified within AWS SES
            ConfigurationSetName: process.env.SNS_EMAIL_CONFIGURATION_SETS,
        }
    }
    await sendEmailToSes(params, raw)
        .then(data => {
            return data;
        })
        .catch(error => {
            console.log(`=== Sent mail errors ${error.message} =====`);
            return error;
        });

}

function sendEmailToSes(params, raw) {
    //exec call to SES as a promise to wait for response
    if (raw) {
        return ses.sendRawEmail(params).promise()
    } else {
        return ses.sendEmail(params).promise()
    }

}


module.exports = {
    sendEmail: sendEmail
}
