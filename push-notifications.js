const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('../config/firebase/firebaseServiceKey');
const GlobalConst = require('../config/global');
const Devices = require('../models').Devices;
const FirebaseAdmin = require('firebase-admin');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
let _ = require('lodash');
const FBMessagingErrors = [
    'The registration token is not a valid FCM registration token',
    'NotRegistered',
    'BadDeviceToken',
    'Unregistered'
];
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

firebaseAdmin.initializeApp({
    projectId: serviceAccount.project_id,
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: GlobalConst.FirebaseConfig.DATABASE_URL,
    serviceAccountId: GlobalConst.FirebaseConfig.SERVICE_ACCOUNT_ID
});

module.exports = {
    sendPushNotification
}

function sendPushNotification(payload) {
    let iosAndroidNotification = Object.assign({}, payload.notification);
    if (typeof iosAndroidNotification.click_action != "undefined") {
        delete iosAndroidNotification.click_action;
    }

    if (typeof payload.notification.title != "undefined") {
        payload.notification.title =  entities.decode(payload.notification.title).replace(/(<([^>]+)>)/ig, "");
    }

    if (typeof payload.notification.body != "undefined") {
        payload.notification.body = entities.decode(payload.notification.body).replace(/(<([^>]+)>)/ig, "");
    }
    const message = {
        notification: iosAndroidNotification,
        android: {
            ttl: 3600 * 1000,
            notification: {
                icon: 'stock_ticker_update',
                color: '#f45342',
                sound: typeof payload.sound == "undefined" ? 'default' : payload.sound
            },
        },
        apns: {
            payload: {
                aps: {
                    badge: typeof payload.badge == "undefined" ? 2 : payload.badge,
                    sound: typeof payload.sound == "undefined" ? 'default' : payload.sound
                },
            },
        },
        webpush: {
            notification: payload.notification
        },
        tokens: _.isString(payload.devicesToken) ? [payload.devicesToken] : payload.devicesToken
    };
    if (typeof payload.contentAvailable != 'undefined') {
        message.apns.payload.aps.contentAvailable = payload.contentAvailable;
    }

    if (typeof payload.mutableContent != 'undefined') {
        message.apns.payload.aps.mutableContent = payload.mutableContent;
    }

    send(message);
}

function send(message) {
    /*
    * Noted : Should maximum 100 token for each push
    */
    return FirebaseAdmin.messaging().sendMulticast(message)
        .then((response) => {
            if (response.failureCount > 0) {
                const failedTokens = [];
                response.responses.forEach((resp, idx) => {
                    if (!resp.success) {
                        failedTokens.push(message.tokens[idx]);
                    }
                });
                deregisterToken(FBMessagingErrors[0], failedTokens);
            }
            return true;
        })
        .catch((error) => {
            deregisterToken(error.message, message.token)
            return false;
        });
}

function deregisterToken(error, devicesToken) {
    if (FBMessagingErrors.includes(error)) {
        console.log(`====== Error push notifications: ${error} with ${devicesToken.length} token failed =======`);
        Devices.update({status: false}, {
            where: {
                deviceToken: {
                    [Op.in]: devicesToken
                }
            }
        });
    }
}
